package edu.upc.damo.toDoList;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by Josep M on 04/11/2015.
 */
public class ListViewMeu extends ListView
        implements ModelObservable.OnCanviModelListener {

    // Constructors exigits pel framework


    public ListViewMeu(Context context, AttributeSet attribute, int defStyleAtt) {
        super(context, attribute, defStyleAtt);
    }

    public ListViewMeu(Context context, AttributeSet attribute) {
       super(context, attribute);
    }


    @Override
    public void onNovesDades() {
        invalidateViews();
    }


    public void setModel(ModelObservable model){     // Alternativa 2
        model.setOnCanviModelListener(this);
    }
}
